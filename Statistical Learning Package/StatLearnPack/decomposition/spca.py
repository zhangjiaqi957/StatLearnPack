import numpy as np

"""
Reference
--------------
Zou, H., Hastie, T. and Tibshirani, R. (2006) "Sparse principal component analysis" Journal of Computational and Graphical Statistics, 15 (2), 265–286.
"""

def spca(x, K, use_corr=False, lambda_ridge=1e-6, max_iter=200, trace=False, eps_conv=1e-3):
    if use_corr == True:
        