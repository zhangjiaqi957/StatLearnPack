import numpy as np
from sklearn.metrics import accuracy_score

def accuracy(y_true, y_predict):
    """
    Function `accuracy` compute classification accuracy.
    
    Parameters
    ----------
    y_true : 1-d array-like
    y_predict : 1-d array-like

    Returns
    ----------
    acc : fraction of correctly classified samples 

    Example
    ----------

    """
    if type(y_true) != np.ndarray:
        y_true = np.asarray(y_true)
    if type(y_predict) != np.ndarray:
        y_predict = np.asarray(y_predict)
    y_true = y_true.reshape(-1, )
    y_predict = y_predict.reshape(-1, )
    if y_true.shape[0] != y_predict.shape[0]:
        ValueError("y_treu and y_predict should have same length!")
    n = y_true[0]
    acc = np.sum(y_true == y_predict) / n
    return acc

def R_Squared(y, y_hat):
    SSR = np.sum(np.power(y - y_hat, 2))
    SST = np.sum(np.power(y - np.mean(y), 2))
    R_Square = SSR/SST
    return R_Square