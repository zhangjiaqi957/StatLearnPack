class Data_length_Error(Exception):  # 继承异常类
    def __init__(self, n1, n2,x1_name,x2_name):
        self.n1 = n1
        self.n2 = n2
        self.x1_name = x1_name
        self.x2_name = x2_name
    def __str__(self):
        return "Data_length_Error: the data length of len({0})={2}!=len({1})={3}".format(self.x1_name,self.x2_name,self.n1, self.n2,)
