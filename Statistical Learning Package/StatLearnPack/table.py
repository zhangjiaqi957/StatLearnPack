class simple_table(object):

    def __init__(self, title=None, columns=None, unit_width=20):
        self.columns = columns
        self.__info_container = []
        self.unit_width = unit_width
        self.title = title
        self.max_row_width = 0
        self.max_unit_width = 0
        self.columns = 0
        self.rows = 0
        self.shape = (self.rows, self.columns)
        self.__line_pos = []
    
    def add_row(self, row):
        self.__info_container.append(row)
    
    def __str__(self) -> str:
        print_str = ''
        print_str = print_str + self.__padding_title()
        pos_index = 0
        self.__get_max_unit_width()
        for index, row in enumerate(self.__info_container):
            if len(self.__line_pos)!=0:
                pos = self.__line_pos[pos_index][0]
                style = self.__line_pos[pos_index][1]
                if index == pos:
                    pos_index = pos_index + 1  

                if style == 'double':
                    print_str = print_str + self.__add_line('double')
                elif style == 'single':
                    print_str = print_str + self.__add_line('single')

            for item in row:
                print_str = print_str + self.__padding_unit(item)
            print_str = print_str + '\n'
        if len(self.__line_pos) > pos_index:
            pos_index = pos_index + 1
            if style == 'double':
                print_str = print_str + self.__add_line('double')
            elif style == 'single':
                print_str = print_str + self.__add_line('single')
        return print_str

    def __padding_unit(self, item) -> str:

        item = str(item)
        item_length = len(item)
        if item_length < self.unit_width:
            left = (self.unit_width - item_length)//2
            right = (self.unit_width - left - item_length)
            padding_item = ' ' * left + item + ' '*right
        else: 
            padding_item = ' ' + item + " "
        return padding_item
    
    def __get_max_row_unit(self):
        """获取最大单元格数量"""
        max_unit = 0
        for row in self.__info_container:
            if len(row)>max_unit:
                max_unit = len(row)
        return max_unit
    
    def __get_columns_and_rows(self):
        self.columns = max([len(row) for row in self.__info_container])
        self.rows = len(self.__info_container)


    def __get_max_row_width(self) -> int:
        self.max_row_width = 0
        row_width = 0

        self.__get_max_unit_width()
        for row in self.__info_container:
            row_width = 0
            for e in row:
                if len(str(e)) < self.unit_width:
                    row_width = row_width + self.unit_width
                else:
                    # self.__padding_unit 函数
                    # 对于超出预设宽度的单元格左右两边加空格
                    # 因此需在此处再加上两个空格的长度
                    row_width = row_width + len(str(e)) + 2 
            
            if self.max_row_width < row_width:
                self.max_row_width = row_width



        return self.max_row_width

    def __get_max_unit_width(self):
        max_unit_width = 0
        for row in self.__info_container:
            
            for e in row:
                if max_unit_width<len(str(e)):
                    max_unit_width = len(str(e))
        self.max_unit_width = max_unit_width
        return max_unit_width
    
    def add_line(self, style='double'):
        pos = len(self.__info_container)
        self.__line_pos.append((pos, style))

    def __add_line(self, style='double') -> str:
        if style=='double':
            line_str = '=' 
        elif style=='single':
            line_str = '-'
        line = line_str * (self.max_row_width) + '\n'
        self.__get_columns_and_rows()
        return line

    def __padding_title(self) -> str:
        total_length = self.__get_max_row_width()
        left = (total_length - len(self.title)) // 2
        right = total_length - left - len(self.title)
        padding_title = ' ' * left + self.title + ' '*right + '\n'
        return padding_title