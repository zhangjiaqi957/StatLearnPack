from setuptools import setup, find_packages

setup(
    name="StatLearnPack",
    version="1.0",
    author="ZHANG Jiaqi",
    author_email="zhangjiaqi957975@outlook.com",
    description="Statistical Learning Package is a Open Source Package for Statistical Computing",
    packages=find_packages(),
    url="https://gitee.com/zhangjiaqi957/Statistics-Open-Source-Package",
    license='Apache-2.0'
    )