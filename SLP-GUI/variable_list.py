from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os


class var_list_widget(QtWidgets.QTableView):

    def __init__(self):
        super(var_list_widget, self).__init__()
        self.init_var_list()
        self.var_list = []

    def init_var_list(self):
        self.setWindowTitle("变量列表")
        self.horizontalHeader().setDefaultAlignment(QtCore.Qt.AlignCenter)
        self.var_model = QtGui.QStandardItemModel(0, 2)
        self.var_model.setHorizontalHeaderLabels(["变量名", "变量类型"])
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.setModel(self.var_model)
        self.show()
    
    def add_items_by_row(self, var_and_type: list):
        var_item = QtGui.QStandardItem(str(var_and_type[0]))
        type_item = QtGui.QStandardItem(str(var_and_type[1]))

        var_item.setTextAlignment(QtCore.Qt.AlignCenter)
        type_item.setTextAlignment(QtCore.Qt.AlignCenter)
        
        self.var_model.appendRow([var_item, type_item])
        self.setModel(self.var_model)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Qw = QtWidgets.QWidget()
    main = var_list_widget()
    main.add_item(['1','2','2'])
    main_2 = var_list_widget()
    lay = QtWidgets.QHBoxLayout()
    lay.addWidget(main_2)
    lay.addWidget(main)
    Qw.setLayout(lay)
    Qw.show()

    sys.exit(app.exec_())