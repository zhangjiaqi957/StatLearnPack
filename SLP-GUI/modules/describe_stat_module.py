from itertools import count
from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os
import pandas as pd
from StatLearnPack.general import summary

class describe_stat_dialog(QtWidgets.QWidget):

    # 定义信号
    out_come_signal = QtCore.pyqtSignal(str)
    
    def __init__(self, cur_data:pd.DataFrame):
        super(describe_stat_dialog, self).__init__()
        self.setWindowTitle("描述性统计")
        self.cur_data = cur_data
        self.init_ui()
        
    def init_ui(self):
        #设置窗口默认大小
        self.resize(640,480)
        self.Layout_1 = QtWidgets.QHBoxLayout()
        self.Layout_2 = QtWidgets.QVBoxLayout()
        self.Layout_3 = QtWidgets.QVBoxLayout()

        self.listwidget_1 = QtWidgets.QListWidget()
        self.listwidget_1.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listwidget_1.setSortingEnabled(True)
        self.listwidget_2 = QtWidgets.QListWidget()
        self.listwidget_2.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listwidget_2.setSortingEnabled(True)

        self.listwidget_1.addItems(self.cur_data.columns)

        self.to_right = QtWidgets.QPushButton("选择")
        self.to_right.clicked.connect(self.on_to_right_clicked)
        self.to_left = QtWidgets.QPushButton("选择2")
        self.to_left.clicked.connect(self.on_to_left_clicked)

        self.Layout_2.addWidget(self.to_right)
        self.Layout_2.addWidget(self.to_left)

        self.Layout_1.addWidget(self.listwidget_1)
        self.Layout_1.addLayout(self.Layout_2)
        self.Layout_1.addWidget(self.listwidget_2)

        self.okbtn = QtWidgets.QPushButton("确定")
        self.okbtn.clicked.connect(self.on_okbtn_clicked)
        self.Layout_3.addLayout(self.Layout_1)
        self.Layout_3.addWidget(self.okbtn)
        self.setLayout(self.Layout_3)




    def on_to_right_clicked(self):
        text_list_qt =  list(self.listwidget_1.selectedItems())
        text_list = []
        self.listwidget_1.selectAll()

        for item in text_list_qt:
            text_list.append(item.text())
            self.listwidget_1.takeItem(self.listwidget_1.row(item))
        self.listwidget_2.addItems(text_list)
    
    def on_to_left_clicked(self):
        text_list_qt =  self.listwidget_2.selectedItems()
        index_list = self.listwidget_2.selectedIndexes()
        text_list = []
        for item in text_list_qt:
            text_list.append(item.text())
            self.listwidget_2.takeItem(self.listwidget_2.row(item))
        self.listwidget_1.addItems(text_list)
    
    def on_okbtn_clicked(self):
        text_list = []
        count_item = self.listwidget_2.count()
        for i in range(count_item):
            text_list.append(self.listwidget_2.item(i).text())
        self.outcome_str = summary(self.cur_data[text_list])
        # 发送信号
        self.out_come_signal.emit(self.outcome_str)

if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    data = pd.read_excel(r"C:\Users\zhang\Desktop\科研\Statistics-Open-Source-Package\SLP-GUI\新建 XLSX 工作表.xlsx")
    main = describe_stat_dialog(data)
    main.show()
    sys.exit(app.exec_())

