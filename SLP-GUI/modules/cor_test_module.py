from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os
from StatLearnPack.stat_test import Cor

class cor_test_dialog(QtWidgets.QWidget):

    # 定义信号
    out_come_signal = QtCore.pyqtSignal(str)
    
    def __init__(self, cur_data):
        super(cor_test_dialog, self).__init__()
        self.setWindowTitle("相关系数检验")
        self.cur_data = cur_data
        self.init_ui()
        
    def init_ui(self):
        #设置窗口默认大小
        self.setFixedSize(300,300)
        self.Layout_1 = QtWidgets.QVBoxLayout()
        self.Layout_2 = QtWidgets.QVBoxLayout()
        self.Layout_3 = QtWidgets.QHBoxLayout()
        self.Layout_4 = QtWidgets.QVBoxLayout()

    def init_var_select_box(self, var_list: list):
        #初始化变量选择框
        self.Lab1 = QtWidgets.QLabel("变量1")
        self.Lab2 = QtWidgets.QLabel("变量2")

        self.var_box_1 = QtWidgets.QComboBox()  
        self.var_box_2 = QtWidgets.QComboBox()  
        self.method = QtWidgets.QComboBox()

        self.method.addItems(['pearson', 'spearman'])

        self.var_box_1.addItems(var_list)
        self.var_box_2.addItems(var_list)

        self.Layout_1.addWidget(self.Lab1)
        self.Layout_1.addWidget(self.var_box_1)
        self.Layout_1.addWidget(self.Lab2)
        self.Layout_1.addWidget(self.var_box_2)

        self.Layout_2.addWidget(self.method)

        self.Layout_3.addLayout(self.Layout_1)
        self.Layout_3.addLayout(self.Layout_2)

        
        self.okbtn = QtWidgets.QPushButton("确定")
        self.okbtn.setFixedSize(80,30)
        self.okbtn.clicked.connect(self.on_ok_clicked)

        self.Layout_4.addLayout(self.Layout_3)
        self.Layout_4.setAlignment(QtCore.Qt.AlignCenter)
        self.Layout_4.addWidget(self.okbtn,alignment=QtCore.Qt.AlignHCenter)

        self.setLayout(self.Layout_4)

    def on_ok_clicked(self):
        if self.cur_data.empty:
            self.close()
        else:
            self.var_index_1 = self.var_box_1.currentIndex()
            self.var_index_2 = self.var_box_2.currentIndex()
            
            Cor_test = Cor()
            self.outcome_str = Cor_test.test(self.cur_data.iloc[:, self.var_index_1], self.cur_data.iloc[:, self.var_index_2], method =self.method.currentText())
            # 发送信号
            self.out_come_signal.emit(self.outcome_str)
            self.close()


