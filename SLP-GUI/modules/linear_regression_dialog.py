from itertools import count
from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os
import pandas as pd
from StatLearnPack.general import Summarize
from StatLearnPack.linear_models import Linear_Regression

class describe_stat_dialog(QtWidgets.QWidget):
    # 定义信号
    out_come_signal = QtCore.pyqtSignal(str)

    def __init__(self, cur_data: pd.DataFrame):
        super(describe_stat_dialog, self).__init__()
        self.setWindowTitle("描述性统计")
        self.cur_data = cur_data
        self.init_ui()

    def init_ui(self):
        # 设置窗口默认大小
        self.resize(640, 480)
        self.Layout_1 = QtWidgets.QHBoxLayout()
        self.Layout_2 = QtWidgets.QVBoxLayout()
        self.Layout_3 = QtWidgets.QVBoxLayout()
        self.Layout_4 = QtWidgets.QVBoxLayout() # 包含自变量与因变量选择框

        self.listwidget_all = QtWidgets.QListWidget()
        self.listwidget_all.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listwidget_all.setSortingEnabled(True)

        self.listwidget_x = QtWidgets.QListWidget()
        self.listwidget_x.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listwidget_x.setSortingEnabled(True)

        self.listwidget_y = QtWidgets.QListWidget()
        self.listwidget_y.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listwidget_y.setSortingEnabled(True)
        self.Layout_4.addWidget(self.listwidget_y)
        self.Layout_4.addWidget(self.listwidget_x)


        self.listwidget_all.addItems(self.cur_data.columns)

        self.to_y = QtWidgets.QPushButton("选择")
        self.to_y.clicked.connect(self.on_to_y_clicked)
        self.to_left = QtWidgets.QPushButton("选择")
        self.to_left.clicked.connect(self.on_to_left_clicked)

        self.Layout_2.addWidget(self.to_y)
        self.Layout_2.addWidget(self.to_left)

        self.Layout_1.addWidget(self.listwidget_all)
        self.Layout_1.addLayout(self.Layout_2)
        self.Layout_1.addLayout(self.Layout_4)

        self.okbtn = QtWidgets.QPushButton("确定")
        self.okbtn.clicked.connect(self.on_okbtn_clicked)
        self.Layout_3.addLayout(self.Layout_1)
        self.Layout_3.addWidget(self.okbtn)
        self.setLayout(self.Layout_3)

    def on_to_y_clicked(self):
        text_list_qt = list(self.listwidget_all.selectedItems())
        text_list = []
        for item in text_list_qt:
            text_list.append(item.text())
            self.listwidget_all.takeItem(self.listwidget_all.row(item))
        self.listwidget_y.addItems(text_list)

    def on_to_left_clicked(self):
        text_list_qt = self.listwidget_2.selectedItems()
        index_list = self.listwidget_2.selectedIndexes()
        text_list = []
        for item in text_list_qt:
            text_list.append(item.text())
            self.listwidget_2.takeItem(self.listwidget_2.row(item))
        self.listwidget_1.addItems(text_list)

    def on_okbtn_clicked(self):
        text_list = []
        count_item = self.listwidget_2.count()
        for i in range(count_item):
            text_list.append(self.listwidget_2.item(i).text())
        self.outcome_str = Summarize.summary_table(self.cur_data[text_list])
        # 发送信号
        self.out_come_signal.emit(self.outcome_str)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    data = pd.read_excel(r"C:\Users\zhang\Desktop\科研\Statistics-Open-Source-Package\SLP-GUI\新建 XLSX 工作表.xlsx")
    main = describe_stat_dialog(data)
    main.show()
    sys.exit(app.exec_())

