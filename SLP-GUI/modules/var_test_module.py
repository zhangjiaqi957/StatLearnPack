from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os

class var_test_dialog(QtWidgets.QWidget):
    def __init__(self):
        super(var_test_dialog, self).__init__()
        self.setWindowTitle("方差比检验")

    def setupUI(self):
        self.Layout_1 = QtWidgets.QVBoxLayout(self)

        self.var_box = variable_select_box(self)    
        self.var_box.addItems(['python','c++'])

        self.Layout_1.addWidget(self.var_box)
        self.setLayout(self.Layout_1)

class variable_select_box(QtWidgets.QComboBox):

    def __init__(self):
        super(variable_select_box, self).__init__()
        



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    var = var_test_dialog()
    var.show()
    app.exec_()
