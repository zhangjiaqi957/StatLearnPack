from unicodedata import name
from PyQt5 import QtCore, QtGui, QtWidgets
import sys, os
import matplotlib.pyplot as plt
class data_visualization(QtWidgets.QWidget):

    """
    该模块用于数据可视化分析
    """
    def __init__(self, cur_data):
        super(data_visualization, self).__init__()
        self.setWindowTitle("绘图")
        self.cur_data = cur_data
        self.init_ui()
    
    def init_ui(self):
        self.graph_type = QtWidgets.QComboBox()
        self.method.addItems(['散点图', '折线图'])

        self.Layout_1 = QtWidgets.QVBoxLayout()

        self.Lab1 = QtWidgets.QLabel("x")
        self.Lab2 = QtWidgets.QLabel("y")

        self.var_box_1 = QtWidgets.QComboBox()  
        self.var_box_2 = QtWidgets.QComboBox()  

        self.var_box_1.addItems(self.cur_data.columns)
        self.var_box_2.addItems(self.cur_data.columns)
        
        self.Layout_1.addWidget(self.Lab1)
        self.Layout_1.addWidget(self.var_box_1)
        self.Layout_1.addWidget(self.Lab2)
        self.Layout_1.addWidget(self.var_box_2)

        self.okbtn = QtWidgets.QPushButton("确定")
        self.okbtn.setFixedSize(80,30)

        self.Layout_1.addWidget(self.okbtn)
        self.setLayout(self.Layout_1)

    def on_okbtn_click(self):
        pass

if __name__ =='__main__':
    import pandas as pd
    app = QtWidgets.QApplication(sys.argv)
    data = pd.read_excel(r"C:\Users\zhang\Desktop\科研\Statistics-Open-Source-Package\SLP-GUI\新建 XLSX 工作表.xlsx")
    main = data_visualization(data)
    main.show()
    sys.exit(app.exec_())