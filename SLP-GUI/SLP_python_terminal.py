
from PyQt5 import QtCore, QtGui, QtWidgets

class out_command_line(QtWidgets.QTextEdit):

    def __init__(self):
        super(out_command_line, self).__init__()
        self.setReadOnly(True)
        self.setWindowTitle("输出窗口")
        self.setFont(QtGui.QFont('Courier New', 10))
        self.Prompt = "Out:"
        self.str_doc = "̀Copyright (C) 2021-2022 The StatLearnPack Project Developers.\n\n"+\
                       "SLP is a free software for statistical computing without any guarantee, " +\
                       "you can start your analyze by using GUI or python package StatLearnPack. "+\
                       "This is cmmand line of SLP Create by ZHANG Jia-qi 2021-12-20.\n\n"+\
                        "See more detail in website: https://gitee.com/zhangjiaqi957/Statistics-Open-Source-Package.git \n""" 
        self.setText(self.str_doc)
        self.text_cursor = self.textCursor()
        self.text_cursor.movePosition(QtGui.QTextCursor.End)
        self.setTextCursor(self.text_cursor)

        self.line_num = self.document().lineCount()
        QtCore.QMetaObject.connectSlotsByName(self)
        self.setReadOnly(True)


    def add_out_info(self, out_info):
        self.str_doc = self.str_doc + out_info
        self.setText(self.str_doc)

"""
    def keyPressEvent(self, event):

        super().keyPressEvent(event)
        # !TODO: print(event.key() , QtCore.Qt.Key_Enter) why they are not equal when enter pressed?
        if event.key() == 16777220:
            str_doc = self.document().toPlainText() + self.Prompt + ' '
            self.setText(str_doc)
            self.text_cursor.movePosition(QtGui.QTextCursor.End)
            self.setTextCursor(self.text_cursor)
            self.line_num += 1
        elif event.key() == 16777219:
            self.textCursor().set(3)
            self.on_last_line_edit()

    def mousePressEvent(self, event):

        super().mousePressEvent(event)
        if event.buttons() == QtCore.Qt.LeftButton:
            self.on_last_line_edit()

    def on_last_line_edit(self):
        "you can only edit the last line in cmd line"
        if self.document().blockCount()==self.line_num and self.textCursor().positionInBlock()>=3:
            self.setReadOnly(False)
        else:
            self.setReadOnly(True)
"""
