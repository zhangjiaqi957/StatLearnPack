# Statistical Learning Package

[![GitHub license](https://img.shields.io/badge/License-Apache--2.0-brightgreen)](./LICENSE)
![Python Version](https://img.shields.io/badge/Python-%3E%3D3.6-orange)

# Introduction
StatLearnPack 是一个用于统计建模的算法包。支持假设检验，聚类分析，线性模型等基本统计分析方法。

<div align=center><img src = logo.png width="35%"></div>

项目分为 `StatLearnPack` 与 `SLP-GUI` 两部分。`StatLearnPack` 支持在Python脚本中以第三方包的方式调用常见统计算法，`SLP-GUI` 基于 `PyQT5` 构建，支持以图形界面的方式调用算法并在界面命令行中进行输出。

# Installation

安装当前版本的 `StatLearnPack` 可使用如下命令进行安装。

```{shell}
git clone https://gitee.com/zhangjiaqi957/StatLearnPack.git
cd "Statistical Learning Package"
pip install -e .
```

随后在 SLP-GUI 目录下运行 `main.py` 可启动StatLearnPack GUI。

# StatLearnPack.stat_test

+ [ttest](#statlearnpackstattestttest)
+ [vartest](#statlearnpackstattestvartest)
+ [Boxtest](#statlearnpackstattestboxtest)

## StatLearnPack.stat_test.ttest

*ttest(x, y=None, mu=0, var_equal=True, conf_level= 0.95,paired=False, alternative='two_sided')*

`x , y` : array_like

`mu` ： 检验值

`var_equal`: True 假定方差齐， False 假定方差不齐，对自由度进行修正

`conf_level` : 置信度 （0 < conf_level < 1）

`paired` : 当需要使用两配对样本 t 检验时，设置该选项为 True

`alternative` : 包含三种选项

+ "two_sided" : 
    + 单样本 t 检验 ： $ H_{a} : \bar{x} = \mu $
    + 两独立样本 t 检验 ： $H_{a} : \bar{x} - \bar{y} = \mu $ 
    + 两配对样本 t 检验 ： $ H_{a} : \bar{x} - \bar{y} = \mu $
+ "less"
    + 单样本 t 检验 ： $ H_{a} : \bar{x} < \mu $
    + 两独立样本 t 检验 ： $H_{a} : \bar{x} - \bar{y} < \mu $ 
    + 两配对样本 t 检验 ： $ H_{a} : \bar{x} - \bar{y} < \mu $
+ "greater"
    + 单样本 t 检验 ： $ H_{a} : \bar{x} > \mu $
    + 两独立样本 t 检验 ： $H_{a} : \bar{x} - \bar{y} > \mu $ 
    + 两配对样本 t 检验 ： $ H_{a} : \bar{x} - \bar{y} > \mu $

### Examples : 

当仅输入 x 时，该函数会进行单样本 t 检验。

```python
>>> # One-Sample t-test
>>> from StatLearnPack import stat_test
>>> import numpy as np
>>> x = np.array([1,2,3])
>>> stat_test.ttest(x)
Out:
                   One Sample t-test
========================================================
 t = 3.4641   df = 2   p-value = 0.0742  num of obs = 3
 95.0 percent confidence interval :   (-0.4841, 4.4841)
 sample estimates :  mean of x    2.0
========================================================
```

如果同时输入 x 与 y ，那么该函数会进行两独立样本的 t 检验，并且默认方差相等。考虑方差不等的情况可使用选项 `var_equal = False`

```python
>>> # two independent sample t-test
>>> from StatLearnPack import stat_test 
>>> import numpy as np
>>> x = np.array([1,2,3])
>>> y = np.array([1,2,3])
>>> print(stat_test.ttest(x,y,var_equal=True)) # var_equal = TRUE is default option
Out:
                  Welch Two Sample t-test
===========================================================
 t = -1.7913  df = 12   p-value = 0.0985  num of obs = 7 7
 95.0 percent confidence interval :   (-4.116, 0.4017)
 sample estimates :   mean of x 2.4286  mean of y 4.2857
===========================================================
```

如果设置了 `paired = True` 则会进行两配对样本的 t 检验。

```python
>>> # two related samples t-test
>>> from StatLearnPack import stat_test 
>>> import numpy as np
>>> x = np.array([1,2,3,3,2,4,2])
>>> y = np.array([1,2,3,4,5,7,8])
>>> print(stat_test.ttest(x,y, paired=True))
Out:
                      Paired t-test
=========================================================
 t = -2.1667   df = 6   p-value = 0.0734  num of obs = 7
 95.0 percent confidence interval :   (-3.9545, 0.2402)
 sample estimates :  mean of x  -1.8571
=========================================================
```

## StatLearnPack.stat_test.vartest

*vartest(x, y, ratio=1, alternative='tow_sided', conf_level=0.95)*

该函数用于比较两正态总体的方差。检验统计量为

$$
F = \frac{S_{x}^{2}}{S_{y}^{2}}\sim F(n_{1} - 1, n_{2}-1)
$$
其中 $n_{1},n_{2}$分别为 $x,y$ 两组数据的样本容量 $S_{x}^{2} = \frac{1}{n_{1}-1}(x_{i}-\bar{x})^{2}$, $S_{x}^{2} = \frac{1}{n_{2}-1}(y_{i}-\bar{y})^{2}$，$\bar{x}, \bar{y}$ 分别为样本均值。

`x, y` : array_like

`ration` : 检验比例

`alternative` ： 备择假设

`conf_level` : 置信度

### Examples ：
```python
>>> # F test to compare the variances of two samples from normal populations
>>> x = np.array([1,2,3,5,7,10,2,3,11,20,9,2,-5,0,11,100,1000,2000,-1500,2000])
>>> y = np.array([2,4,7,20,1,-5,0,25,30,17])
>>> print(stat_test.vartest(x, y, alternative='less'))
Out:
                F test to compare two variances
================================================================
 F = 3889.5339 num df = 19  denom = 9   p-value = 1.0
 alternative hypothesis: true ratio of variances is less than 1
 95 percent confidence interval:  (0, 9423.1696)
 sample estimates:  ratio of variances
================================================================
```

## StatLearnPack.stat_test.boxtest

*boxtest(x, lag, type)*

`x` : array_like

`lag` : 滞后阶数

`type` : 检验类型，包含如下两种检验方式
            
+ 'Box-Pierce'
+ 'Ljung-Box'

### Examples ：

```python
>>> from StatLearnPack.stat_test import boxtest
>>> x = [1,2,3,4,5,6,7,8]
>>> print(boxtest(x,1,type='Box-Pierce'))
Out:
            Box-Pierce test
========================================
 X-squared = 3.125         df = 1       
  p-value = 0.0771  
========================================
```
## StatLearnPack.stat_test.proptest

*proptest(x, n, p=0.5, alternative='tow_sided', conf_level=0.95, correct=True)*

`x` : 事件发生的次数

`n` : 总样本量

`p` : 检验概率

`alternative` : 备择假设

`conf_level` : 置信度（默认95%）

`correct` : 设置为 `True`在计算 $\chi^{2}$ 统计量时采用 Yate 连续性修正

# models模块

```{python}
Linear_regression
```
这个类中使用了普通最小二乘法（OLS）对模型参数进行估计。下面是我们的例子

```{python}
>>> from sklearn.datasets import load_diabetes
>>> model = Linear_Regression()
>>> model.fit(data_diabetes['data'], data_diabetes['target'])
>>> model.summary()
"""
Out:
                                                OLS Summary Tabel
==============================================================================================================================
    R-Square =             0.6746        Number of obs =             442
    SSR =             2621009.1244             df =                  10       
    SSE =             1263983.1563             df =                  431
    SST =             3884992.2807             df =                  441   
    F statistic =        89.3726             Prob > F                0.0
------------------------------------------------------------------------------------------------------------------------------
    variable               coef                   t                 P > |t|              Std.Err         95% Conf.Interval
------------------------------------------------------------------------------------------------------------------------------
        x1                -10.0122              -0.1676              0.5665               59.7492        [-14.6969  -5.3275]
        x2                -239.8191             -3.9172              0.9999               61.2223       [-244.6193 -235.0189]
        x3                519.8398              7.8132                 0.0                66.5336        [514.6231 525.0564]
        x4                324.3904              4.9584                 0.0                65.4219        [319.2609 329.5199]
        x5                -792.1842             -1.9012               0.971              416.6839       [-824.8548 -759.5135]
        x6                476.7458              1.4062               0.0802              339.0345        [450.1634 503.3283]
        x7                101.0446              0.4754               0.3174              212.5326        [ 84.3807 117.7085]
        x8                177.0642              1.0965               0.1367              161.4756        [164.4035 189.7249]
        x9                751.2793              4.3704                 0.0                171.902        [737.8011 764.7575]
        x10                67.6254              1.0249                0.153               65.9842         [62.4518 72.799 ]
    intercept            152.1335              59.0614                0.0                2.5759         [151.9315 152.3354]
==============================================================================================================================
"""
```


